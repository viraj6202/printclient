package printclient.controls;

import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import printclient.log.ExLogger;

public class Database {

    public static SystemTray stray;

    public static TrayIcon icon;

    public static Connection con;

    public static Connection conAss;

    static {
        try {
            con = getCon();
            conAss = getAssCon();
        } catch (Exception ex) {
            icon.displayMessage("Print Client", "Database connection fail", TrayIcon.MessageType.ERROR);
        }
    }

    public static Connection getCon() {
        try {
            if (con == null) {
                System.out.println("No connection, connecting..");
                con = getDatabaseConnection();
            }
            if (con.isClosed()) {
                System.out.println("connection has been closed. reconnecting...");
                con = getDatabaseConnection();
            }
            return con;
        } catch (Exception e) {
            ExLogger.logError(e);
            return null;
        }
    }

    private static Connection getDatabaseConnection() throws Exception {
        Properties prop = new Properties();
        prop.load(new FileInputStream("config.properties"));
        String host = prop.getProperty("host");
        String database_name = prop.getProperty("database_name");
        String database_user = prop.getProperty("database_user");
        String database_password = prop.getProperty("database_password");
        Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection("jdbc:mysql://" + host + ":3306/" + database_name + "?autoReconnect=true", database_user, database_password);
    }

    public static Connection testCon() {
        try {
            Properties prop = new Properties();
            prop.load(new FileInputStream("config.properties"));
            String host = prop.getProperty("host");
            String database_name = prop.getProperty("database_name");
            String database_user = prop.getProperty("database_user");
            String database_password = prop.getProperty("database_password");
            Class.forName("com.mysql.jdbc.Driver");
            return DriverManager.getConnection("jdbc:mysql://" + host + ":3306/" + database_name + "?autoReconnect=true", database_user, database_password);
        } catch (Exception e) {
            return null;
        }
    }

    public static Connection getAssCon() {
        try {
            if (conAss != null) {
                return conAss;
            }
            Properties prop = new Properties();
            prop.load(new FileInputStream("config.properties"));
            String host = prop.getProperty("host");
            String database_name_ass = prop.getProperty("database_name_ass");
            String database_user = prop.getProperty("database_user");
            String database_password = prop.getProperty("database_password");
            Class.forName("com.mysql.jdbc.Driver");
            return DriverManager.getConnection("jdbc:mysql://" + host + ":3306/" + database_name_ass  + "?autoReconnect=true", database_user, database_password);
        } catch (Exception e) {
            return null;
        }
    }

    public static Connection testAssCon() {
        try {
            Properties prop = new Properties();
            prop.load(new FileInputStream("config.properties"));
            String host = prop.getProperty("host");
            String database_name_ass = prop.getProperty("database_name_ass");
            String database_user = prop.getProperty("database_user");
            String database_password = prop.getProperty("database_password");
            Class.forName("com.mysql.jdbc.Driver");
            return DriverManager.getConnection("jdbc:mysql://" + host + ":3306/" + database_name_ass + "?autoReconnect=true", database_user, database_password);
        } catch (Exception e) {
            return null;
        }
    }

    public static ResultSet getData(String sql) throws Exception {
        Statement state = con.createStatement();
        ResultSet rset = state.executeQuery(sql);
        return rset;
    }

    public static boolean isExist(String sql) throws Exception {
        boolean Exist = false;
        Statement state = con.createStatement();
        ResultSet rset = state.executeQuery(sql);
        if (rset.next()) {
            Exist = true;
        }
        return Exist;
    }
}
