package printclient.log;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author viraj
 */
public class ExLogger {
        private static PrintStream ps;

    static {
        try {
            ps = new PrintStream(new FileOutputStream(System.getProperty("user.home") + "/error.log"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExLogger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void logError(Exception $ex) {
        StackTraceElement[] stackTrace = $ex.getStackTrace();
        if (ps != null) {
            $ex.printStackTrace(ps);
        }
    }
}
