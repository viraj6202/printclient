package printclient.client;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;

public class Ticket {

    private String contentTicket = "{{line1}}\n";

    Ticket(String linr_1) {
        this.contentTicket = this.contentTicket.replace("{{line1}}", linr_1);
    }

    public void print() {
        DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;

        PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
        PrintService[] printService = PrintServiceLookup.lookupPrintServices(flavor, pras);
        PrintService defaultService = PrintServiceLookup.lookupDefaultPrintService();

        byte[] bytes = this.contentTicket.getBytes();

        Doc doc = new SimpleDoc(bytes, flavor, null);

        DocPrintJob job = defaultService.createPrintJob();

        try {
            job.print(doc, null);
        } catch (Exception er) {
        }
    }
}
