package printclient.client;

import javax.swing.JLabel;

public class PropertiesEditor extends javax.swing.JDialog {

    private static final long serialVersionUID = -3734087049387760990L;
    private javax.swing.JTextField database;
    private javax.swing.JTextField hostname;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private JLabel jLabel1;
    private JLabel jLabel2;

    public PropertiesEditor(java.awt.Frame parent, boolean modal) /*     */ {
        super(parent, modal);
        initComponents();
        loadConfig();
        this.port.setText("1337");
        this.jButton1.setEnabled(false);
    }

    private void loadConfig() {
        try {
            java.util.Properties p = Config.getConfiguration();
            this.hostname.setText(p.getProperty("host"));
            this.port.setText(p.getProperty("port"));
            this.database.setText(p.getProperty("database_name"));
            this.username.setText(p.getProperty("database_user"));
            this.password.setText(p.getProperty("database_password"));
        } catch (Exception ex) {
            System.out.println("here");
        }
    }

    private void resetSystem() {
        this.hostname.setText("");
        this.port.setText("1337");
        this.database.setText("");
        this.username.setText("");
        this.password.setText("");
        this.hostname.requestFocusInWindow();
    }

    public java.sql.Connection testCon() {
        try {
            String host = this.hostname.getText();
            String database_name = this.database.getText();
            String database_user = this.username.getText();
            String database_password = this.password.getText();

            Class.forName("com.mysql.jdbc.Driver");
            return java.sql.DriverManager.getConnection("jdbc:mysql://" + host + ":3306/" + database_name, database_user, database_password);
        } catch (Exception e) {
        }
        return null;
    }

    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLabel8;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JPasswordField password;
    private javax.swing.JFormattedTextField port;
    private javax.swing.JTextField username;

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.hostname = new javax.swing.JTextField();
        this.jLabel2 = new JLabel();
        this.port = new javax.swing.JFormattedTextField();
        this.jLabel4 = new JLabel();
        this.jLabel5 = new JLabel();
        this.jLabel6 = new JLabel();
        this.database = new javax.swing.JTextField();
        this.username = new javax.swing.JTextField();
        this.password = new javax.swing.JPasswordField();
        this.jSeparator2 = new javax.swing.JSeparator();
        this.jButton1 = new javax.swing.JButton();
        this.jButton2 = new javax.swing.JButton();
        this.jLabel8 = new JLabel();
        this.jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(2);
        setTitle("Connection Configuration");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                PropertiesEditor.this.formWindowClosed(evt);
            }

        });
        this.jLabel1.setText("Hostname/IP : ");

        this.jLabel2.setText("Port : ");

        this.port.setEditable(false);
        this.port.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        this.port.setText("1337");

        this.jLabel4.setText("Database : ");

        this.jLabel5.setText("Username : ");

        this.jLabel6.setText("Password : ");

        this.jButton1.setText("Save");
        this.jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PropertiesEditor.this.jButton1ActionPerformed(evt);
            }

        });
        this.jButton2.setText("Reset");
        this.jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PropertiesEditor.this.jButton2ActionPerformed(evt);
            }

        });
        this.jLabel8.setBackground(new java.awt.Color(0, 102, 102));
        this.jLabel8.setFont(new java.awt.Font("Segoe UI Semilight", 0, 18));
        this.jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        this.jLabel8.setText("Connection Settings");
        this.jLabel8.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 3, 0, new java.awt.Color(255, 102, 0)), javax.swing.BorderFactory.createEmptyBorder(15, 20, 10, 20)));
        this.jLabel8.setOpaque(true);

        this.jButton3.setText("Test Connection");
        this.jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PropertiesEditor.this.jButton3ActionPerformed(evt);
            }

        });
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout
                .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(this.jSeparator2)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(this.jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(this.jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(this.jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(this.jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(this.jLabel2, javax.swing.GroupLayout.Alignment.TRAILING))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(this.hostname)
                                                .addComponent(this.database)
                                                .addComponent(this.username)
                                                .addComponent(this.password)
                                                .addComponent(this.port)))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGap(0, 0, 32767)
                                        .addComponent(this.jButton2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(this.jButton3, -2, 119, -2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(this.jButton1)))
                        .addContainerGap())
                .addComponent(this.jLabel8, -1, 350, 32767));

        layout.setVerticalGroup(layout
                .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addComponent(this.jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(this.jLabel1)
                                .addComponent(this.hostname, -2, -1, -2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(this.jLabel2)
                                .addComponent(this.port, -2, -1, -2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(this.jLabel4)
                                .addComponent(this.database, -2, -1, -2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(this.jLabel5)
                                .addComponent(this.username, -2, -1, -2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(this.jLabel6)
                                .addComponent(this.password, -2, -1, -2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(this.jSeparator2, -2, 10, -2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, -1, 32767)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(this.jButton1)
                                .addComponent(this.jButton2)
                                .addComponent(this.jButton3))
                        .addContainerGap()));

        setSize(new java.awt.Dimension(366, 283));
        setLocationRelativeTo(null);
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            java.util.Properties prop = new java.util.Properties();
            prop.setProperty("host", this.hostname.getText().trim());
            prop.setProperty("port", this.port.getText().trim());
            prop.setProperty("option", String.valueOf(1));
            prop.setProperty("database_name", this.database.getText().trim());
            prop.setProperty("database_user", this.username.getText().trim());
            prop.setProperty("database_password", new String(this.password.getPassword()));
            Config.setConfiguration(prop);
            javax.swing.JOptionPane.showMessageDialog(this, "Configuration created successfully.", "Success", 1);
            resetSystem();
            this.jButton1.setEnabled(false);
        } catch (Exception e) {
            javax.swing.JOptionPane.showMessageDialog(this, e.getMessage(), "Error", 1);
            e.printStackTrace();
        }
    }

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
        resetSystem();
    }

    private void formWindowClosed(java.awt.event.WindowEvent evt) {
        System.exit(0);
    }

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {
        if (testCon() == null) {
            this.jButton1.setEnabled(false);
            javax.swing.JOptionPane.showMessageDialog(this, "Database Connection Error", "Database Connection", 0);
        } else {
            this.jButton1.setEnabled(true);
            javax.swing.JOptionPane.showMessageDialog(this, "Successfully Connected", "Database Connection", 1);
        }
    }

    public static void main(String[] args) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PropertiesEditor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                PropertiesEditor dialog = new PropertiesEditor(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    public void windowClosing(java.awt.event.WindowEvent e) {
                    }

                });
                dialog.setVisible(true);
            }
        });
    }
}
