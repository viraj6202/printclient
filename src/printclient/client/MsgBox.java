package printclient.client;

import printclient.controls.Database;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.util.HashMap;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle;
import printclient.log.ExLogger;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;

public class MsgBox extends Dialog {

    int bill;

    private JButton OK;

    private JButton jButton1;

    private Label lblBillNo;

    public MsgBox(Frame parent, boolean modal, int billNo) {
        super(parent, modal);
        initComponents();
        toFront();
        requestFocus();
        this.OK.requestFocus();
        this.bill = billNo;
        preparePrint();
    }

    private void preparePrint() {
        this.lblBillNo.setText(String.valueOf(this.bill));
    }

    public void printBill(int billNo) {
        try {
            Connection con = Database.getCon();
            if (con.isClosed()) {
                Database.icon.displayMessage("Network connection error", "Please check the network connection or \n PLease restart the systrm", TrayIcon.MessageType.ERROR);
                return;
            }
            String path = "bill/bill.jrxml";
            JasperReport jr = JasperCompileManager.compileReport(path);
            HashMap<String, Object> params = new HashMap<>();
            params.put("payId", Integer.valueOf(billNo));
            JasperPrint fr = JasperFillManager.fillReport(jr, params, con);
            boolean print = false;
            if (fr != null) {
                print = JasperPrintManager.printReport(fr, false);
            }
            dispose();
        } catch (Exception ex) {
            ex.printStackTrace();
            ExLogger.logError(ex);
            JOptionPane.showMessageDialog(this, "No data found in given criteriauuuuuu", "Print Client Printing Error", JOptionPane.ERROR_MESSAGE);
//            Database.icon.displayMessage("Print Client Printing Error", "No data found in given criteria", TrayIcon.MessageType.ERROR);
        }
    }

    public static void getPrintData(int i) {
    }

    private void initComponents() {
        this.OK = new JButton();
        this.jButton1 = new JButton();
        this.lblBillNo = new Label();
        setAlwaysOnTop(true);
        setResizable(false);
        addWindowListener(new WindowAdapter() {
            public void windowActivated(WindowEvent evt) {
                MsgBox.this.formWindowActivated(evt);
            }

            public void windowClosing(WindowEvent evt) {
                MsgBox.this.closeDialog(evt);
            }
        });
        this.OK.setText("Print Bill");
        this.OK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                MsgBox.this.OKActionPerformed(evt);
            }
        });
        this.jButton1.setText("Cancell");
        this.jButton1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                MsgBox.this.jButton1ActionPerformed(evt);
            }
        });
        this.lblBillNo.setFont(new Font("Dialog", 0, 16));
        this.lblBillNo.setName("");
        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);
        layout.setHorizontalGroup(layout
                .createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(27, 27, 27)
                                        .addComponent(this.lblBillNo, -1, 141, 32767))
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(23, 23, 23)
                                        .addComponent(this.OK, -2, 69, -2)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(this.jButton1)))
                        .addContainerGap(33, 32767)));
        layout.setVerticalGroup(layout
                .createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(this.lblBillNo, -2, 32, -2)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(this.OK, -1, 37, 32767)
                                .addComponent(this.jButton1, -1, -1, 32767))
                        .addContainerGap()));
        pack();
        setLocationRelativeTo((Component) null);
    }

    private void closeDialog(WindowEvent evt) {
        setVisible(false);
        dispose();
    }

    private void OKActionPerformed(ActionEvent evt) {
        printBill(this.bill);
    }

    private void formWindowActivated(WindowEvent evt) {
        this.OK.requestFocus();
    }

    private void jButton1ActionPerformed(ActionEvent evt) {
        dispose();
    }

    public static void main(String[] args) {
    }
}
