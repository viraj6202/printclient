/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package printclient;

import printclient.client.MsgBox;
import printclient.client.Config;
import printclient.controls.Database;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.imageio.ImageIO;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.UIManager;
import printclient.log.ExLogger;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

public class PrintClient {

    public static SystemTray systemTray;
    public static TrayIcon trayIcon;
    public static int portNumber = 0;


    public PrintClient() {
        initServer(portNumber);
    }

    private void initServer(int port) {
        try {
            Properties prop = new Properties();
            prop.load(new FileInputStream("config.properties"));
            port = Integer.parseInt(prop.getProperty("port"));
            
            if (SystemTray.isSupported()) {
                systemTray = SystemTray.getSystemTray();
                BufferedImage read = ImageIO.read(new File("report-icon-13324.png"));

                trayIcon = new TrayIcon(read);

                trayIcon.setImageAutoSize(true);

                trayIcon.setToolTip("Print Client");

                trayIcon.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        PrintClient.trayIcon.displayMessage("Print Client", "Welcome! This computer is equipped with print client.", TrayIcon.MessageType.INFO);
                    }
                });
                PopupMenu menu = new PopupMenu();
                MenuItem updt = new MenuItem("Check Update");
                updt.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                    }
                });
                menu.add(updt);
                MenuItem prnt = new MenuItem("Available Printers");
                prnt.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
                        String[] myarray = new String[20];
                        int i = 0;
                        for (PrintService printer : printServices) {
                            myarray[i] = printer.getName();
                            i++;
                        }
                        final JFrame f = new JFrame("Availablew Printers");
                        JList<String> list = new JList<>(myarray);
                        f.setPreferredSize(new Dimension(300, 200));
                        f.setResizable(false);
                        f.add(list);
                        f.pack();
                        f.setIconImage((new ImageIcon("report-icon-13324.png")).getImage());
                        f.setDefaultCloseOperation(2);
                        f.setLocationRelativeTo((Component) null);
                        f.setVisible(true);
                        list.addMouseListener(new MouseAdapter() {
                            public void mouseClicked(MouseEvent evt) {
                                JList list = (JList) evt.getSource();
                                if (evt.getClickCount() >= 1) {
                                    f.dispose();
                                }
                            }
                        });
                    }
                });
                menu.add(prnt);
                MenuItem mi = new MenuItem("About");
                mi.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        JOptionPane.showMessageDialog(null, "PrintClient Reports\nDeveloped by MDCC IT Solutions", "About", -1);
                    }
                });
                menu.add(mi);
                MenuItem printTest = new MenuItem("Receipt re-print");
                printTest.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        String id = JOptionPane.showInputDialog(null, "Enter pay id", "Receipt re-print", 3);
                        if (id == null) {
                            return;
                        }
                        PrintClient.this.printBill(Integer.parseInt(id));
                    }
                });
                menu.add(printTest);
                MenuItem ex = new MenuItem("Exit");
                ex.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        int i = JOptionPane.showConfirmDialog(null, "Do you need to exit?", "Exit", 0, 3);
                        if (i == 0) {
                            System.exit(0);
                        }
                    }
                });
                menu.add(ex);

                trayIcon.setPopupMenu(menu);

                systemTray.add(trayIcon);
            }
            startClient(port);
        } catch (Exception e) {
//      e.printStackTrace();
            ExLogger.logError(e);
        }
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            if (!Config.isValidConfigurationFound()) {
                Config.createConfig();
                System.exit(0);
            }
            new PrintClient();
        } catch (Exception e) {
//            e.printStackTrace();
            ExLogger.logError(e);

        }
    }

    private void startClient(int port) {
        try {
            ServerSocket ss = new ServerSocket(port);

            trayIcon.displayMessage("Report Client", "Welcome! This computer is equipped with print client.", TrayIcon.MessageType.INFO);
            while (true) {
                Socket client = ss.accept();
                BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                PrintWriter out = new PrintWriter(client.getOutputStream());
                out.print("HTTP/1.1 200 \r\n");
                out.print("Access-Control-Allow-Origin: *\r\n");
                out.print("Content-Type: text/plain\r\n");
                out.print("Connection: close\r\n");
                out.print("\r\n");
                String line;
                if ((line = in.readLine()) != null) {
                    if (line.length() == 0) {
                        break;
                    }
                    System.out.println(line);
                    out.print(line + "\r\n");

                    String[] split = line.split(" ", 3);
                    String reqStr = split[1].substring(2);
                    String[] reqVars = reqStr.split("&");
                    int dataId = Integer.parseInt(reqVars[1].split("=")[1]);
                    int typeId = Integer.parseInt(reqVars[3].split("=")[1]);

                    if (typeId == 1) {
                        try {
                            MsgBox w = new MsgBox(null, true, dataId);
                            w.setVisible(true);
                            w.requestFocus();
                        } catch (Exception e) {
                            System.out.println(dataId);
                            ExLogger.logError(e);
                            break;
                        }
                    } else if (typeId == 5) {
                        try {
                            rePrintBill(dataId);
                        } catch (Exception e) {
                            ExLogger.logError(e);
                            break;
                        }
                    }
                }
                out.close();
                in.close();
                client.close();
            }
        } catch (IOException | NumberFormatException e) {
//            e.printStackTrace();
            ExLogger.logError(e);
            System.err.println(e);
            System.err.println("Usage: java HttpMirror <port>");
            trayIcon.displayMessage("Print Client", "Report sending failed.", TrayIcon.MessageType.ERROR);
        }
    }

    private void rePrintBill(int billNo) {
        try {
            Connection con = Database.getCon();
            if (Database.testCon() == null) {

                trayIcon.displayMessage("Network connection error", "Please check the network connection or \n PLease restart the systrm", TrayIcon.MessageType.ERROR);
                return;
            }
            int proId = billTypeForSpecificBill(billNo);
            String path = "";
            if (proId == 1) {
                path = "bill/bill_rathnapura_asstax.jrxml";
            }
            JasperReport jr = JasperCompileManager.compileReport(path);
            HashMap<String, Object> params = new HashMap<>();
            params.put("payId", Integer.valueOf(billNo));
            JasperPrint fr = JasperFillManager.fillReport(jr, params, con);
            boolean print = false;
            if (fr != null) {
                print = JasperPrintManager.printReport(fr, false);
            }
        } catch (Exception ex) {
            ExLogger.logError(ex);
            trayIcon.displayMessage("Print Client Printing Error", "No data found in given criteria", TrayIcon.MessageType.ERROR);
        }
    }

    private void printBillInSubject(int billNo) {
        try {
            Connection con = Database.getCon();
            if (Database.testCon() == null) {

                trayIcon.displayMessage("Network connection error", "Please check the network connection or \n PLease restart the systrm", TrayIcon.MessageType.ERROR);
                return;
            }
            int proId = billTypeForSpecificBill(billNo);
            String path = "";
            if (proId == 1) {
                path = "bill/bill_rathnapura_asstax.jrxml";
            } else if (proId == 3) {
                path = "bill/bill_rathnapura_trade_tax.jrxml";
            }
            JasperReport jr = JasperCompileManager.compileReport(path);
            HashMap<String, Object> params = new HashMap<>();
            params.put("payId", Integer.valueOf(billNo));
            JasperPrint fr = JasperFillManager.fillReport(jr, params, con);
            boolean print = false;
            if (fr != null) {
                print = JasperPrintManager.printReport(fr, false);
            }
        } catch (Exception ex) {
            ExLogger.logError(ex);
            trayIcon.displayMessage("Print Client Printing Error", "No data found in given criteria", TrayIcon.MessageType.ERROR);
        }
    }

    private int billTypeForSpecificBill(int bill) {
        int type = 0;
        try {
            Connection con = Database.getCon();
            if (Database.testCon() == null) {

                trayIcon.displayMessage("Network connection error", "Please check the network connection or \n PLease restart the systrm", TrayIcon.MessageType.ERROR);
            }
            PreparedStatement ps = con.prepareStatement("SELECT payments.pro_id FROM `payments` WHERE payments.pay_id = ?");
            ps.setInt(1, bill);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                type = rs.getInt("pro_id");
            }
        } catch (Exception exception) {
            ExLogger.logError(exception);

        }
        return type;
    }

    public void printBill(int billNo) {
        try {
            Connection con = Database.getCon();
            if (Database.testCon() == null) {
                trayIcon.displayMessage("Network connection error", "Please check the network connection or \n PLease restart the systrm", TrayIcon.MessageType.ERROR);
                return;
            }
            String path = "bill/bill.jrxml";
            JasperReport jr = JasperCompileManager.compileReport(path);
            HashMap<String, Object> params = new HashMap<>();
            params.put("payId", Integer.valueOf(billNo));
            JasperPrint fr = JasperFillManager.fillReport(jr, params, con);
            boolean print = false;
            if (fr != null) {
                print = JasperPrintManager.printReport(fr, false);
            }
        } catch (Exception ex) {
            ExLogger.logError(ex);
            trayIcon.displayMessage("Print Client Printing Error", "No data found in given criteria", TrayIcon.MessageType.ERROR);
        }
    }
}
